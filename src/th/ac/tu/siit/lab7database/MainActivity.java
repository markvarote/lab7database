package th.ac.tu.siit.lab7database;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends ListActivity {
	
	DBHelper dbHelper;
	SQLiteDatabase db;
	Cursor cursor; // keep the data retrieved from the database
	SimpleCursorAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		dbHelper = new DBHelper(this);
		db = dbHelper.getWritableDatabase();
		cursor = getAllContacts();
		adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, 
				new String[] {"ct_name", "ct_phone", "ct_type", "ct_email"},
				new int[] {R.id.tvName, R.id.tvPhone, R.id.ivPhoneType, R.id.tvEmail}, 0);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}
	
	private Cursor getAllContacts() {
		//db.query is a method for executing SELECT statement
		return db.query("contacts", //table name
				new String[] {"_id", "ct_name", "ct_phone",
				"ct_type", "ct_email"}, //list of column names
				null, //condition for WHERE clause, e.g. "ct_name LIKE?"
				null, //values for the conditions, e.g. new String []{"John%"}
				null, //GROUP BY
				null, //HAVING
				"ct_name asc" //ORDER BY
				);
		//SELECT _id , ct_name, ct_phone, ct_type, ct_email FROM contacts
		//ORDER BY ct_name ASC;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.context, menu);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		cursor.close();
		db.close();
		dbHelper.close();
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, 
			int resultCode, Intent data) {
		if (requestCode == 9999 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			db.insert("contacts", null, v);//Executing INSERT statement
			//Refresh the ListView to show the updated record
			//(1) re-retrieve all the records(create a new cursor)
			cursor = getAllContacts();
			//(2) set the new cursor to the adapter
			adapter.changeCursor(cursor);
			//(3) call notifyDataSetChanged to update the data
			adapter.notifyDataSetChanged();
			
		}
		else if(requestCode == 8888 && resultCode == RESULT_OK) {
			ContentValues v = new ContentValues();
			v.put("ct_name", data.getStringExtra("name"));
			v.put("ct_phone", data.getStringExtra("phone"));
			v.put("ct_email", data.getStringExtra("email"));
			v.put("ct_type", data.getStringExtra("type"));
			String selection ="_id = ?";
			String [] selectionArgs={String.valueOf(data.getLongExtra("id", -1))};
			db.update("contacts", v,selection,selectionArgs);
			//(1) re-retrieve all the records(create a new cursor)
			cursor = getAllContacts();
			//(2) set the new cursor to the adapter
			adapter.changeCursor(cursor);
			//(3) call notifyDataSetChanged to update the data
			adapter.notifyDataSetChanged();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			Intent i = new Intent(this, AddNewActivity.class);
			startActivityForResult(i, 9999);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterContextMenuInfo a = (AdapterContextMenuInfo)item.getMenuInfo();
		long id = a.id;
		int position = a.position;
		
		switch(item.getItemId()) {
		case R.id.action_edit:
			//Retrieve the selected record
			Cursor c = (Cursor)adapter.getItem(position);
			//Retrieve the value of ct_name column
			String name = c.getString(c.getColumnIndex("ct_name"));
			String phone = c.getString(c.getColumnIndex("ct_phone"));
			String email = c.getString(c.getColumnIndex("ct_email"));
			int itype = c.getInt(c.getColumnIndex("ct_type"));
			//Create an Intent to send the data to AddNewActivity
			Intent i = new Intent(this, AddNewActivity.class);
			i.putExtra("name", name);
			i.putExtra("phone", phone);
			i.putExtra("email", email);
			i.putExtra("type", itype);//int
			i.putExtra("id", id);//long
			startActivityForResult(i,8888);
			Toast t = Toast.makeText(this, "Selected ID = "+id+
					" with name = "+name, Toast.LENGTH_LONG);
			t.show();
			return true;
		case R.id.action_delete:
			
			String selection = "_id = ?";
			String[] selectionArgs = { String.valueOf(id) };
			db.delete("contacts", selection, selectionArgs);
			
			cursor = getAllContacts();
			adapter.changeCursor(cursor);
			adapter.notifyDataSetChanged();
			
			return true;
		}
		return super.onContextItemSelected(item);
	}
}
